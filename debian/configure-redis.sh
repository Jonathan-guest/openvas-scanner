#!/bin/sh

umask 007
KB_SOCK_PATH=/var/run/redis/redis-server.sock
SCAND_CONFIG_PATH=/etc/openvas/openvassd.conf
REDIS_CONFIG_PATH=/etc/redis/redis.conf
STR_PATTERN="[A-Za-z0-9._/-]*"

prepare_file() {
	FILE=$1
	if [ -f "$FILE" ] ; then
		cp -a "$FILE" `mktemp "$FILE.debian-openvas.XXXXX"`
	else
		touch "$FILE"
	fi
}

maybe_replace() {
	MATCH=$1
	LINE=$2
	FILE=$3

	grep -q "$MATCH" "$FILE" && sed -i -e "s|$MATCH|$LINE|" "$FILE"
}

replace_or_append() {
	KW=$1
	SEP=$2
	VAL=$3
	FILE=$4
	LINE=$KW$SEP$VAL

	maybe_replace "^$KW *$SEP *$STR_PATTERN *$" "$LINE" "$FILE" || \
		maybe_replace "^#$KW *$SEP *$STR_PATTERN *$" "$LINE" "$FILE" || \
		printf "\n$LINE\n" >> "$FILE"
}

process_file() {
	FILE=$1
	SEP=$2
	shift 2

	prepare_file "$FILE"
	while [ $# -gt 0 ]; do
		KW=$1
		VAL=$2
		replace_or_append "$KW" "$SEP" "$VAL" "$FILE"
		shift 2
	done
}

process_file "$SCAND_CONFIG_PATH" "=" \
	kb_location "$KB_SOCK_PATH"

process_file "$REDIS_CONFIG_PATH" " " \
	port 0 \
	unixsocket "$KB_SOCK_PATH" \
	unixsocketperm 700 \
	timeout 0

service redis restart
